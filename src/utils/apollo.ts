import { ApolloClient, InMemoryCache, NormalizedCacheObject, HttpLink} from '@apollo/client'
import { concatPagination } from '@apollo/client/utilities'
import { useMemo } from 'react'

let apolloClient: ApolloClient<NormalizedCacheObject>

function createApolloClient() {
    return new ApolloClient({
        ssrMode: typeof window === 'undefined',
        link: new HttpLink({uri:'http://localhost:1337/graphql'}),
        cache: new InMemoryCache({
            typePolicies: {
              Query: {
                fields: {
                  articles: concatPagination()
                }
              }
            }
          }),
    })
}

export function initializeApollo(initialState = {}){
    const ApolloClientGlobal = apolloClient ?? createApolloClient()

    //Recupera cache
    if(initialState){
        ApolloClientGlobal.cache.restore(initialState)
    }

    //inicializa SSR com cache limpo
    if(typeof window === 'undefined') return ApolloClientGlobal

    apolloClient = apolloClient ?? ApolloClientGlobal

    return apolloClient
}

export function useApollo(initialState = {}) {
    const store = useMemo(() => initializeApollo(initialState), [initialState])
    return store
}