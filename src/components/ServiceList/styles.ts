import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.section`
  ${({ theme }) => css`
    padding-top: ${theme.spacings.large};

    ${media.greaterThan('large')`
      flex-direction: row;
      padding: ${theme.spacings.xlarge} ${theme.spacings.medium} 0;
    `}
  `}
`
export const IconsContainer = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-template-columns: 1fr 1fr;
    text-align: center;
    column-gap: ${theme.spacings.small};
    row-gap: ${theme.spacings.medium};
    margin-top: ${theme.spacings.large};

    ${media.greaterThan('small')`
      grid-template-columns: repeat(auto-fill, minmax(12rem, 1fr));
    `}

    ${media.greaterThan('medium')`
      column-gap: ${theme.spacings.xxlarge};
      row-gap: ${theme.spacings.large};
    `};
  `}
`