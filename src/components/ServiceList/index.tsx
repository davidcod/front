import Heading from "components/Heading";
import ServiceCard, { ServiceCardProps } from "components/ServiceCard";

import * as S from "./styles";

export type ServicesProps = {
  items: ServiceCardProps[];
};

const ServiceList = ({ items }: ServicesProps) => (
  <S.Wrapper>
    <Heading lineLeft lineColor="secondary">
      Our services
    </Heading>
    <S.IconsContainer>
      {items.map((item, index) => (
        <ServiceCard key={index} {...item} />
      ))}
    </S.IconsContainer>
  </S.Wrapper>
);

export default ServiceList;
