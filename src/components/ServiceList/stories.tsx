import { Story, Meta } from "@storybook/react";
import ServiceList, { ServiceListProps } from ".";

export default {
  title: "ServiceList",
  component: ServiceList,
  args: {
    name: "Service name",
    icon: "icon",
    linkLabel: "Learn more",
    url: "/services",
  },
} as Meta;

export const Default: Story<ServiceListProps> = (args) => (
  <ServiceList {...args} />
);
