import { render, screen } from '@testing-library/react'

import ServiceList from '.'

describe('<ServiceList />', () => {
  it('should render the heading', () => {
    const { container } = render(<ServiceList />)

    expect(screen.getByRole('heading', { name: /ServiceList/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
