import { render, screen } from '@testing-library/react'

import Links from '.'

describe('<Links />', () => {
  it('should render the heading', () => {
    const { container } = render(<Links />)

    expect(screen.getByRole('heading', { name: /Links/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
