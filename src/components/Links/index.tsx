import Link from "next/link";
import * as S from "./styles";

export type LinksProps = {
  name: string;
  url: string;
};

const Links = ({ name, url }: LinksProps) => (
  <S.Wrapper>
    <Link href={url} passHref>
      <S.MenuLink>{name}</S.MenuLink>
    </Link>
  </S.Wrapper>
);

export default Links;
