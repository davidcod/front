import { Story, Meta } from '@storybook/react'
import Links from '.'

export default {
  title: 'Links',
  component: Links
} as Meta

export const Default: Story = () => <Links />
