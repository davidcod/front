import * as S from "./styles";

export type BlockTextProps = {
  title?: string;
  text?: string;
  image?: string;
  rtl?: boolean;
};

const BlockText = ({ title, text, image, rtl }: BlockTextProps) => (
  <S.Wrapper>
    <S.BlockContent rtl={rtl}>
      <S.BlockText>
        <S.Title>{title}</S.Title>
        <S.Text>{text}</S.Text>
      </S.BlockText>
      <S.BlockImage>
        <S.Image src={image} alt={title} />
      </S.BlockImage>
    </S.BlockContent>
  </S.Wrapper>
);

export default BlockText;
