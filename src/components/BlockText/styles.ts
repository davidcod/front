import styled, { css } from 'styled-components'
import { BlockTextProps } from '.'

// eslint-disable-next-line @typescript-eslint/ban-types
export type WrapperProps = {} & Pick<BlockTextProps, 'rtl'>

const wrapperModifiers = {
  rtl: () => css`
    flex-direction: row-reverse;
  `
}

export const Wrapper = styled.section`
  ${({ theme }) => css`
    max-width: 100%;
  `}
`
export const BlockContent = styled.div<WrapperProps>`
  ${({ rtl }) => css`
    display: flex;
    justify-content: space-between;

    ${rtl && wrapperModifiers.rtl()};
  `}
`
export const Title = styled.h1`
  ${({ theme }) => css`
    color: ${theme.colors.darkGray};
    margin: 0 0 ${theme.spacings.xxsmall} 0;
    size: ${theme.font.sizes.large};
  `}
`
export const Text = styled.p`
  ${({ theme }) => css`
    color: ${theme.colors.darkGray};
    size: ${theme.font.sizes.xsmall};
  `}
`

export const BlockImage = styled.div``
export const Image = styled.img``
export const BlockText = styled.div`
  ${({ theme }) => css`
    padding: ${theme.spacings.xsmall};
  `}
`
