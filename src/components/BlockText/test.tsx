import { render, screen } from '@testing-library/react'

import BlockText from '.'

describe('<BlockText />', () => {
  it('should render the heading', () => {
    const { container } = render(<BlockText />)

    expect(screen.getByRole('heading', { name: /BlockText/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
