import { Story, Meta } from '@storybook/react'
import BlockText, { BlockTextProps } from '.'

export default {
  title: 'BlockText',
  component: BlockText,
  args: {
    title: 'Lorem ipsum dolor sit amet',
    image: 'https://source.unsplash.com/user/willianjusten/850x420',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu risus ultricies ligula efficitur volutpat nec sit amet dui. Phasellus a ultrices augue.',
    rtl: false
  }
} as Meta

export const Default: Story<BlockTextProps> = (args) => <BlockText {...args} />
