import styled, {css} from 'styled-components'
import media from 'styled-media-query'

export const CardContent = styled.div`
    ${({theme}) => css`
        color: ${theme.colors.primary};
        padding: ${theme.spacings.xsmall};
        background: ${theme.colors.white};   
        margin-right: ${theme.spacings.xsmall};

        ${media.lessThan('small')`            
            margin-right: 0;
        `}
    `}
`

export const CardLink = styled.a`
    ${({theme}) => css`
        color: ${theme.colors.primary};
        text-decoration: none; 
        cursor: pointer;
    `}
`

export const CardImage = styled.img`
    height: 18rem;   
    object-fit: cover;   
    width: 100%;
`
export const InfosBlock = styled.div`
    ${({theme}) =>css`
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: ${theme.spacings.xxsmall} 0;
    `}    
`

export const AuthorBlock = styled.div`
    display: flex;
    align-items: center;
`

export const AuthorImage = styled.img`
    height: 2rem;   
    border-radius: 50%;   
    width: 2rem !important;
    margin-right: 1rem;
`


export const AuthorName = styled.div`
    ${({theme}) =>css`
        color: ${theme.colors.gray};
        font-size: ${theme.font.sizes.xsmall};
    `}
`

export const Category = styled.div`
    ${({theme}) =>css`
        background: ${theme.colors.secondary};
        padding: ${theme.spacings.xxsmall};
        border-radius: ${theme.border.radius};
        color: ${theme.colors.white};
        font-size: ${theme.font.sizes.xsmall};
    `}
`

export const CardTitle = styled.h1`
    ${({theme}) => css`
        font-size: ${theme.font.sizes.xxlarge};
        margin-top: ${theme.spacings.small};
        margin-bottom: ${theme.spacings.xxsmall};
    `}
`

export const CardDescription = styled.p`
    ${({theme}) => css`        
        font-size: ${theme.font.sizes.medium};
    `}
`