import Link from "next/link";
import { Grid } from "components/Grid";
import * as S from "./styles";

export type CardProps = {
  img?: string;
  title?: string;
  description?: string;
  slug?: string;
  nameAuthor?: string;
  photoAuthor?: string;
  category?: string;
};

const Card = ({
  img,
  title,
  description,
  slug,
  nameAuthor,
  photoAuthor,
  category,
}: CardProps) => (
  <Grid>
    <S.CardContent>
      <Link href={`article/${slug}`}>
        <S.CardLink>
          <S.CardImage src={img} alt={title} />
        </S.CardLink>
      </Link>
      <S.InfosBlock>
        <S.AuthorBlock>
          <S.AuthorImage src={photoAuthor} alt={nameAuthor} />
          <S.AuthorName>{nameAuthor}</S.AuthorName>
        </S.AuthorBlock>
        <S.Category>{category}</S.Category>
      </S.InfosBlock>
      <Link href={`article/${slug}`}>
        <S.CardLink>
          <S.CardTitle>{title}</S.CardTitle>
        </S.CardLink>
      </Link>
      <S.CardDescription>{description}</S.CardDescription>
    </S.CardContent>
  </Grid>
);

export default Card;
