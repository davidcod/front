import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.section`
  ${({ theme }) => css`
    display: flex;    
    margin-top: ${theme.spacings.large};   

    ${media.greaterThan('medium')`
      padding: ${theme.spacings.large} 0;
    `}
  `}
`

export const Container = styled.div`
  ${({ theme }) => css`
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: ${theme.spacings.medium};    

    ${media.greaterThan('large')`
    grid-template-columns: 1fr 1fr;
      padding: 0 ${theme.spacings.medium};
    `}
  `}
`

export const Image = styled.img`
  max-width: 100%;
  display: block;
  max-width: min(60rem, 100%);
  margin: 0 auto;

  ${media.lessThan('medium')`
    max-width: 100%;
  `}
`

export const SubTitle = styled.div`
${({theme}) =>css`
  color: ${theme.colors.gray};
  margin-top: ${theme.spacings.xsmall};
`}
`

export const Text = styled.div`
  ${({ theme }) => css`
    margin-top: ${theme.spacings.medium};

    p {
      color: ${theme.colors.darkGray};
      margin-bottom: ${theme.spacings.small};
      line-height: 2.4rem;
    }

    strong {
      border-bottom: 2px solid ${theme.colors.primary};
    }
  `}
`
