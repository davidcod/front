import Heading from "components/Heading";
import { Container } from "components/Container";
import { getImageUrl } from "utils/getImageUrl";

import * as S from "./styles";

export type SectionAboutProps = {
  title: string;
  subTitle: string;
  description: string;
  image: string;
};

const SectionAboutProject = ({
  title,
  subTitle,
  description,
  image,
}: SectionAboutProps) => (
  <S.Wrapper>
    <Container>
      <S.Container>
        <S.Image src={image} alt={title} />
        <div>
          <Heading lineLeft color="black">
            {title}
          </Heading>
          <S.SubTitle>{subTitle}</S.SubTitle>
          <S.Text>
            <p>{description}</p>
          </S.Text>
        </div>
      </S.Container>
    </Container>
  </S.Wrapper>
);

export default SectionAboutProject;
