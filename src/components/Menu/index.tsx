import Link from "next/link";

import { useState } from "react";
import { Menu2 as MenuIcon } from "@styled-icons/remix-fill/Menu2";
import { CloseOutline as CloseIcon } from "@styled-icons/evaicons-outline/CloseOutline";

import Logo from "components/Logo";
import MediaMatch from "components/MediaMatch";

import * as S from "./styles";

const Menu = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <S.Wrapper isOpen={isOpen}>
      <MediaMatch lessThan="medium">
        <S.IconWrapper onClick={() => setIsOpen(true)}>
          <MenuIcon aria-label="Open Menu" />
        </S.IconWrapper>
      </MediaMatch>

      <S.LogoWrapper>
        <Link href="/" passHref>
          <a>
            <Logo />
          </a>
        </Link>
      </S.LogoWrapper>

      <MediaMatch greaterThan="medium">
        <S.MenuNav>
          <Link href="/" passHref>
            <S.MenuLink>Home</S.MenuLink>
          </Link>
          <Link href="/about" passHref>
            <S.MenuLink>About</S.MenuLink>
          </Link>
          <Link href="/blog" passHref>
            <S.MenuLink>Blog</S.MenuLink>
          </Link>
          <Link href="/contact" passHref>
            <S.MenuLink>Contact</S.MenuLink>
          </Link>
        </S.MenuNav>
      </MediaMatch>
      <S.MenuFull aria-hidden={!isOpen} isOpen={isOpen}>
        <CloseIcon aria-label="Close menu" onClick={() => setIsOpen(false)} />

        <S.MenuNav>
          <Link href="/" passHref>
            <S.MenuLink>Home</S.MenuLink>
          </Link>
          <Link href="/about" passHref>
            <S.MenuLink>About</S.MenuLink>
          </Link>
          <Link href="/blog" passHref>
            <S.MenuLink>Blog</S.MenuLink>
          </Link>
          <Link href="/contact" passHref>
            <S.MenuLink>Contact</S.MenuLink>
          </Link>
        </S.MenuNav>
      </S.MenuFull>
    </S.Wrapper>
  );
};

export default Menu;
