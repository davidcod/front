import { getImageUrl } from "utils/getImageUrl";
import * as S from "./styles";

export type ServiceCardProps = {
  name: string;
  image: string;
  description: string;
};

const ServiceCard = ({ name, image, description }: ServiceCardProps) => (
  <S.Wrapper>
    <S.Icon>
      <S.Icons src={getImageUrl(image)} alt={name} />
      <S.IconsName>{name}</S.IconsName>
    </S.Icon>
  </S.Wrapper>
);
export default ServiceCard;
