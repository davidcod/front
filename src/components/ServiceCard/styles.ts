import styled, {css} from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.section`
  display: grid;
  //grid-template-columns: repeat(6, 1fr);
  grid-auto-flow: column;
`



export const Icon = styled.div`
  
`

export const Icons = styled.img`
  width: 9rem;
  height: 9rem;
  transition: all 0.4s;
  
`

export const IconsName = styled.p`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.xsmall};
    margin-top: ${theme.spacings.xxsmall};
    color: ${theme.colors.white};
    text-transform: uppercase;
    font-weight: ${theme.font.bold};    
  `}
`

export const IconDescription = styled.p`
  ${({ theme }) => css`
    font-size: ${theme.font.sizes.xsmall};
    margin-top: ${theme.spacings.xxsmall};
    color: ${theme.colors.white};   
  `}
`