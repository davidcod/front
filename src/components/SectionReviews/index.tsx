import { Container } from "components/Container";
import Heading from "components/Heading";
import ReviewCard, { ReviewCardProps } from "components/ReviewCard";
import Slider, { SliderSettings } from "components/Slider";

import * as S from "./styles";

export type ReviewSliderProps = {
  items: ReviewCardProps[];
};

const settings: SliderSettings = {
  dots: true,
  arrows: false,
  slidesToShow: 2,
  infinite: true,
  speed: 500,
  rows: 2,
  slidesPerRow: 1,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        rows: 2,
        slidesPerRow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

const SectionReviews = ({ items }: ReviewSliderProps) => (
  <Container>
    <Heading lineLeft lineColor="primary" color="black">
      Our testimonials
    </Heading>

    <S.Content>
      <Slider settings={settings}>
        {items.map((item) => (
          <ReviewCard key={item.name} {...item} />
        ))}
      </Slider>
    </S.Content>
  </Container>
);

export default SectionReviews;
