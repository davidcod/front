import React, { useEffect } from "react";
import ResizeObserver from "resize-observer-polyfill";
import { getImageUrl } from "utils/getImageUrl";

import * as S from "./styles";

export type ReviewCardProps = {
  name: string;
  image: string;
  description?: string;
};

const ReviewCard = ({ name, image, description }: ReviewCardProps) => {
  useEffect(() => {
    const texts = document.querySelectorAll("p.description");

    const observer = new ResizeObserver((entries) => {
      for (const entry of entries) {
        entry.target.classList[
          entry.target.scrollHeight > entry.contentRect.height + 25
            ? "add"
            : "remove"
        ]("truncated");
      }
    });

    texts.forEach((text) => observer.observe(text));
  });

  return (
    <S.Card>
      <S.User>
        <S.Image src={getImageUrl(image)} alt={name} />
        <S.Name>{name}</S.Name>
      </S.User>
      <S.Text>
        <p className="description">{description}</p>
      </S.Text>
    </S.Card>
  );
};

export default ReviewCard;
