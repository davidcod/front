import * as S from "./styles";
import Button from "components/Button";

export type LinksProps = {
  label: string;
  url: string;
};

export type SectionCTAProps = {
  title?: string;
  text?: string;
  links: LinksProps[];
};

const SectionCta = ({ title, text, links }: SectionCTAProps) => (
  <S.Wrapper>
    <S.Title>{title}</S.Title>
    <S.Text>{text}</S.Text>
    <S.SectionButton>
      {links.map((link) => (
        <Button href={link.url} size="large" as={"a"}>
          {link.label}
        </Button>
      ))}
    </S.SectionButton>
  </S.Wrapper>
);

export default SectionCta;
