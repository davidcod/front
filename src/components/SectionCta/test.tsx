import { render, screen } from '@testing-library/react'

import SectionCta from '.'

describe('<SectionCta />', () => {
  it('should render the heading', () => {
    const { container } = render(<SectionCta />)

    expect(screen.getByRole('heading', { name: /SectionCta/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
