import { Story, Meta } from '@storybook/react'
import SectionCta from '.'

export default {
  title: 'SectionCta',
  component: SectionCta
} as Meta

export const Default: Story = () => <SectionCta />
