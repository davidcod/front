import styled, {css} from 'styled-components'

import * as ButtonStyles from 'components/Button/styles'

export const Wrapper = styled.section`
    display: block;
`
export const Title = styled.h1`
    ${({theme}) =>css` 
        font-size: ${theme.font.sizes.xxlarge};
        margin-bottom: ${theme.spacings.xsmall};
    `}
`

export const Text = styled.p``

export const SectionButton = styled.div`
    ${({theme}) =>css` 
        margin-top: ${theme.spacings.large};

        ${ButtonStyles.Wrapper} {
            background: ${theme.colors.primary};
            transition: ${theme.transition.default};

            &:hover{
                background: transparent;
                border: 1px solid ${theme.colors.primary};
                color: ${theme.colors.primary};
            }
        }
        
    `}
`

