import { Story, Meta } from "@storybook/react";
import { ArrowRight } from "@styled-icons/bootstrap/ArrowRight";
import Button, { ButtonProps } from ".";

export default {
  title: "Button",
  component: Button,
  argTypes: {
    children: {
      type: "string",
    },
    size: {
      control: {
        type: "select",
        options: ["small", "medium", "large"],
      },
    },
    minimal: {
      control: {
        type: "boolean",
      },
    },
    icon: {
      type: "",
    },
  },
} as Meta;

export const Default: Story<ButtonProps> = (args) => <Button {...args} />;

Default.args = {
  children: "Buy now",
};

export const withIcon: Story<ButtonProps> = (args) => <Button {...args} />;

withIcon.args = {
  size: "small",
  children: "Learn more",
  icon: <ArrowRight />,
};

export const asLink: Story<ButtonProps> = (args) => <Button {...args} />;

asLink.args = {
  size: "large",
  children: "Learn more",
  as: "a",
  href: "/link",
};
