import { useState, InputHTMLAttributes, TextareaHTMLAttributes } from "react";

import * as S from "./styles";

export type TextAreaFieldProps = {
  onInputChange?: (value: string) => void;
  onInput?: (value: string) => void;
  label?: string;
  initialValue?: string;
  disabled?: boolean;
  error?: string;
} & Omit<TextareaHTMLAttributes<HTMLTextAreaElement>, "onInput">;

const TextAreaField = ({
  label,
  name,
  initialValue = "",
  error,
  disabled = false,
  onInput,
  onInputChange,
  ...props
}: TextAreaFieldProps) => {
  const [value, setValue] = useState(initialValue);

  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newValue = e.currentTarget.value;
    setValue(newValue);

    !!onInput && onInput(newValue);
  };

  return (
    <S.Wrapper disabled={disabled} error={!!error}>
      {!!label && <S.Label htmlFor={name}>{label}</S.Label>}
      <S.InputWrapper>
        <S.TextArea
          onChange={onChange}
          value={value}
          disabled={disabled}
          rows={10}
          name={name}
          {...(label ? { id: name } : {})}
          {...props}
        />
      </S.InputWrapper>
      {!!error && <S.Error>{error}</S.Error>}
    </S.Wrapper>
  );
};

export default TextAreaField;
