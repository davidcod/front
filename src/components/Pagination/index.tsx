import * as S from "./styles";

export type PaginationProps = {
  items: string[];
};

const Pagination = ({ items }: PaginationProps) => (
  <S.Wrapper>
    {items.map((item) => (
      <S.Page>{item}</S.Page>
    ))}
  </S.Wrapper>
);

export default Pagination;
