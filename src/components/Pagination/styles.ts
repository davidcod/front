import styled, {css} from 'styled-components'

export const Wrapper = styled.ul`
    display: flex;
    justify-content: center;
`
export const Page = styled.li`
${({theme}) =>css`
    list-style: none;
    border: 1px solid ${theme.colors.gray};
    padding: ${theme.spacings.xsmall};
`}
`
