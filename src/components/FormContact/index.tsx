import React, { useState } from "react";

import { FieldErrors, submitFormValidate } from "utils/validations";

import { User } from "@styled-icons/boxicons-regular/User";
import { EmailOutline as Email } from "@styled-icons/evaicons-outline/EmailOutline";
import { Subject } from "@styled-icons/material-outlined/Subject";
import {
  CheckCircle,
  CheckCircleOutline,
  ErrorOutline,
} from "styled-icons/material-outlined";

import {
  FormWrapper,
  FormLoading,
  FormError,
  FormSuccess,
} from "components/Form";
import Button from "components/Button";
import TextField from "components/TextField";
import TextAreaField from "components/TextAreaField";

import * as S from "./styles";

const FormContact = () => {
  const [success, setSuccess] = useState(false);
  const [formError, setFormError] = useState("");
  const [fieldError, setFieldError] = useState<FieldErrors>({});
  const [values, setValues] = useState({
    name: "",
    email: "",
    subject: "",
    message: "",
  });
  const [loading, setLoading] = useState(false);

  const handleInput = (field: string, value: string) => {
    setValues((s) => ({ ...s, [field]: value }));
  };

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    setLoading(true);

    const errors = submitFormValidate(values);

    if (Object.keys(errors).length) {
      setFieldError(errors);
      setLoading(false);
      return;
    }

    setFieldError({});

    const response = await fetch("http://localhost:1337/messages", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(values),
    });

    const data = await response.json();
    setLoading(false);

    if (data.error) {
      //setFormError(data.message[0].messages[0].message);
      console.log("error", data);
    } else {
      //setSuccess(true);
      console.log("success", data);
    }
  };

  return (
    <FormWrapper>
      {success ? (
        <FormSuccess>
          <CheckCircleOutline />
          Mensagem enviada com sucesso!
        </FormSuccess>
      ) : (
        <>
          {!!formError && (
            <FormError>
              <ErrorOutline /> {formError}
            </FormError>
          )}
          <form onSubmit={handleSubmit}>
            <TextField
              name="name"
              placeholder="Name"
              onInputChange={(v) => handleInput("name", v)}
              type="text"
              icon={<User />}
            />
            <TextField
              name="email"
              placeholder="E-mail"
              type="text"
              error={fieldError?.email}
              onInputChange={(v) => handleInput("email", v)}
              icon={<Email />}
            />
            <TextField
              name="subject"
              placeholder="Subject"
              onInputChange={(v) => handleInput("subject", v)}
              type="text"
              icon={<Subject />}
            />

            <TextAreaField
              name="message"
              placeholder="Message"
              onInputChange={(v) => handleInput("message", v)}
            />

            <Button size="large" fullWidth>
              {loading ? <FormLoading /> : <span>Send message</span>}
            </Button>
          </form>
        </>
      )}
    </FormWrapper>
  );
};

export default FormContact;
