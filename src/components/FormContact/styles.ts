import styled, { css } from 'styled-components'
import { lighten } from 'polished'
import { TextFieldProps } from 'components/TextField'
import {Input} from 'components/TextField/styles'

type FieldsProps = {} & Pick<TextFieldProps, 'icon' | 'iconPosition'>


export const ForgotPassword = styled.a`
  ${({ theme }) => css`
    display: block;
    font-size: ${theme.font.sizes.small};
    color: ${theme.colors.black};
    text-decoration: none;
    text-align: right;
    &:hover {
      color: ${lighten(0.2, theme.colors.black)};
    }
  `}
`

export const TextAreaField = styled.textarea<FieldsProps>`
  ${({ theme, iconPosition }) => css`
    color: ${theme.colors.black};
    font-family: ${theme.font.family};
    font-size: ${theme.font.sizes.medium};
    padding: ${theme.spacings.xxsmall} 0;
    padding-${iconPosition}: ${theme.spacings.xsmall};
    background: ${theme.colors.lightGray};
    border: 0;
    outline: none;
    width: ${iconPosition === 'right' ? `calc(100% - 2.2rem)` : `100%`};
  `}
`