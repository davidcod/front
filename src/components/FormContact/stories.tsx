import { Story, Meta } from '@storybook/react'
import FormContact from '.'

export default {
  title: 'FormContact',
  component: FormContact
} as Meta

export const Default: Story = () => <FormContact />
