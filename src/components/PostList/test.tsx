import { render, screen } from '@testing-library/react'

import PostList from '.'

describe('<PostList />', () => {
  it('should render the heading', () => {
    const { container } = render(<PostList />)

    expect(screen.getByRole('heading', { name: /PostList/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
