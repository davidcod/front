import Card, { CardProps } from "components/Card";

import * as S from "./styles";

export type PostListProps = {
  items: CardProps[];
};

const PostList = ({ items }: PostListProps) => {
  return (
    <S.Wrapper>
      {items.map((item) => (
        <Card key={item.title} {...item} />
      ))}
    </S.Wrapper>
  );
};

export default PostList;
