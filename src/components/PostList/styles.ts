import styled, {css} from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.main`
    ${({theme}) =>css`
        display: block;        

        ${media.greaterThan("small")`
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(33rem, 1fr));
        grid-gap: ${theme.spacings.medium};
        `}

        img{
            width: 100%;
        }
    `}
   
`
export const ShowMore = styled.button`
    ${({theme}) =>css`        
        border: 1px solid ${theme.colors.white};
        padding: ${theme.spacings.xsmall} ${theme.spacings.small};
        background: ${theme.colors.primary};
        color: ${theme.colors.white};
        transition: ${theme.transition.default};
        cursor: pointer;
        border-radius: ${theme.border.radius};

        &:hover{
            background: ${theme.colors.secondary};
        }
    `}
`
