import { Story, Meta } from '@storybook/react'
import PostList from '.'

export default {
  title: 'PostList',
  component: PostList
} as Meta

export const Default: Story = () => <PostList />
