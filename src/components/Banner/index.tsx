import Image from "next/image";

import Button from "components/Button";

import * as S from "./styles";
import { getImageUrl } from "utils/getImageUrl";

export type BannerProps = {
  img: string;
  title?: string;
  subtitle?: string;
  buttonLabel?: string;
  buttonLink?: string;
};

const Banner = ({
  img,
  title,
  subtitle,
  buttonLabel,
  buttonLink,
}: BannerProps) => (
  <S.Wrapper>
    <S.ImageWrapper>
      <S.Image src={getImageUrl(img)} alt={title} />
    </S.ImageWrapper>

    <S.Caption>
      <S.Title>{title}</S.Title>
      <S.Subtitle>{subtitle}</S.Subtitle>
      {buttonLabel && (
        <Button as="a" href={buttonLink} size="large">
          {buttonLabel}
        </Button>
      )}
    </S.Caption>
  </S.Wrapper>
);

export default Banner;
