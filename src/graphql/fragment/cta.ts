import {gql} from '@apollo/client'

export const CTAFragment = gql`
    fragment CTAFragment on ComponentPageCta{
        title
        text
        links{
            label
            url
        }
    }
`