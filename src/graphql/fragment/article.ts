import {gql} from '@apollo/client'

export const ArticleFragment = gql`
    fragment ArticleFragment on Article{
        title
        slug
        description
        content
        writer{
            name
            photo{
            url
            }
        }
        category{
            name
            slug
        }
        image{
            url
        }
    }

`