import {gql} from '@apollo/client'

export const ReviewFragment = gql`
    fragment ReviewFragment on Review{
        name
        image{
            url
        }
        description
    }
`