import { gql } from '@apollo/client'
import { ReviewFragment } from 'graphql/fragment/review'
import { BannerFragment } from '../fragment/banner'
import { ArticleFragment } from '../fragment/article'
import { ServiceFragment } from '../fragment/service'

export const GET_HOME = gql`
  query GetHome { 
    home{
      banners{
        ...BannerFragment
      }    
      reviews{
        ...ReviewFragment
      } 
      services{
      ...ServiceFragment
      }
      articles{
        ...ArticleFragment
      }
      Sectionabout{
        title
        subTitle
        description
        image{
          url
        }
      }
         
      sectionCTA{
        title
        text
        links{
          label
          url
        }
      }
    }
  }
  ${BannerFragment}
  ${ReviewFragment}
  ${ArticleFragment}
  ${ServiceFragment}
`