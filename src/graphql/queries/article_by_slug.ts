import { gql } from '@apollo/client'

export const ARTICLE_BY_SLUG = gql`
    query ArticleBySlug ($slug: String!){
        articles(where: {slug: $slug}) {
            id
            title
            slug
            content
            category{
                name
            }
            writer{
                name
                photo{
                url
                }
            }
            image{
                url
            }
        }
    }
`

