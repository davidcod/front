import { gql } from '@apollo/client'
import { ReviewFragment } from '../fragment/review'
import { CTAFragment } from '../fragment/cta'

export const GET_ABOUT = gql`
    query GetAbout {
        about{
            title
            slug
            banners{
                image{
                    url
                }
                title
                subtitle
                button{
                    label
                    url
                }
            }
            aboutUs{
                title
                subTitle
                description
                image{
                    url
                }
            }            
            serviceList{
                service{
                    name
                    icon{
                        url
                    }
                }
            }
            
        }
    }
`