import { gql } from '@apollo/client'
import {ArticleFragment} from 'graphql/fragment/article'

export const GET_ARTICLES = gql`
    query GetArticles($start: Int, $limit: Int){
        articles(start: $start, limit: $limit){
            ...ArticleFragment
        }
    }
    ${ArticleFragment}
`