/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetArticles
// ====================================================

export interface GetArticles_articles_writer_photo {
  __typename: "UploadFile";
  url: string;
}

export interface GetArticles_articles_writer {
  __typename: "Writer";
  name: string;
  photo: GetArticles_articles_writer_photo | null;
}

export interface GetArticles_articles_category {
  __typename: "Category";
  name: string;
  slug: string | null;
}

export interface GetArticles_articles_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetArticles_articles {
  __typename: "Article";
  title: string;
  slug: string | null;
  description: string | null;
  content: string | null;
  writer: GetArticles_articles_writer | null;
  category: GetArticles_articles_category | null;
  image: GetArticles_articles_image | null;
}

export interface GetArticles {
  articles: GetArticles_articles[];
}

export interface GetArticlesVariables {
  start?: number | null;
  limit?: number | null;
}
