/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetAbout
// ====================================================

export interface GetAbout_about_banners_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetAbout_about_banners_button {
  __typename: "ComponentPageMenu";
  label: string;
  url: string;
}

export interface GetAbout_about_banners {
  __typename: "Banner";
  image: GetAbout_about_banners_image | null;
  title: string;
  subtitle: string | null;
  button: GetAbout_about_banners_button | null;
}

export interface GetAbout_about_aboutUs_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetAbout_about_aboutUs {
  __typename: "ComponentPageImageText";
  title: string | null;
  subTitle: string | null;
  description: string | null;
  image: GetAbout_about_aboutUs_image | null;
}

export interface GetAbout_about_serviceList_service_icon {
  __typename: "UploadFile";
  url: string;
}

export interface GetAbout_about_serviceList_service {
  __typename: "ComponentPageItems";
  name: string | null;
  icon: GetAbout_about_serviceList_service_icon | null;
}

export interface GetAbout_about_serviceList {
  __typename: "ComponentPageServiceList";
  service: (GetAbout_about_serviceList_service | null)[] | null;
}

export interface GetAbout_about {
  __typename: "About";
  title: string | null;
  slug: string | null;
  banners: GetAbout_about_banners | null;
  aboutUs: GetAbout_about_aboutUs | null;
  serviceList: GetAbout_about_serviceList | null;
}

export interface GetAbout {
  about: GetAbout_about | null;
}
