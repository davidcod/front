/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ReviewFragment
// ====================================================

export interface ReviewFragment_image {
  __typename: "UploadFile";
  url: string;
}

export interface ReviewFragment {
  __typename: "Review";
  name: string | null;
  image: ReviewFragment_image | null;
  description: string | null;
}
