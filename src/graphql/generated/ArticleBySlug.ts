/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ArticleBySlug
// ====================================================

export interface ArticleBySlug_articles_category {
  __typename: "Category";
  name: string;
}

export interface ArticleBySlug_articles_writer_photo {
  __typename: "UploadFile";
  url: string;
}

export interface ArticleBySlug_articles_writer {
  __typename: "Writer";
  name: string;
  photo: ArticleBySlug_articles_writer_photo | null;
}

export interface ArticleBySlug_articles_image {
  __typename: "UploadFile";
  url: string;
}

export interface ArticleBySlug_articles {
  __typename: "Article";
  id: string;
  title: string;
  slug: string | null;
  content: string | null;
  category: ArticleBySlug_articles_category | null;
  writer: ArticleBySlug_articles_writer | null;
  image: ArticleBySlug_articles_image | null;
}

export interface ArticleBySlug {
  articles: ArticleBySlug_articles[];
}

export interface ArticleBySlugVariables {
  slug: string;
}
