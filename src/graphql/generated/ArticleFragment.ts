/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ArticleFragment
// ====================================================

export interface ArticleFragment_writer_photo {
  __typename: "UploadFile";
  url: string;
}

export interface ArticleFragment_writer {
  __typename: "Writer";
  name: string;
  photo: ArticleFragment_writer_photo | null;
}

export interface ArticleFragment_category {
  __typename: "Category";
  name: string;
  slug: string | null;
}

export interface ArticleFragment_image {
  __typename: "UploadFile";
  url: string;
}

export interface ArticleFragment {
  __typename: "Article";
  title: string;
  slug: string | null;
  description: string | null;
  content: string | null;
  writer: ArticleFragment_writer | null;
  category: ArticleFragment_category | null;
  image: ArticleFragment_image | null;
}
