/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetHome
// ====================================================

export interface GetHome_home_banners_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetHome_home_banners_button {
  __typename: "ComponentPageMenu";
  label: string;
  url: string;
}

export interface GetHome_home_banners {
  __typename: "Banner";
  image: GetHome_home_banners_image | null;
  title: string;
  subtitle: string | null;
  button: GetHome_home_banners_button | null;
}

export interface GetHome_home_reviews_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetHome_home_reviews {
  __typename: "Review";
  name: string | null;
  image: GetHome_home_reviews_image | null;
  description: string | null;
}

export interface GetHome_home_services_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetHome_home_services {
  __typename: "Service";
  name: string | null;
  image: GetHome_home_services_image | null;
  description: string | null;
}

export interface GetHome_home_articles_writer_photo {
  __typename: "UploadFile";
  url: string;
}

export interface GetHome_home_articles_writer {
  __typename: "Writer";
  name: string;
  photo: GetHome_home_articles_writer_photo | null;
}

export interface GetHome_home_articles_category {
  __typename: "Category";
  name: string;
  slug: string | null;
}

export interface GetHome_home_articles_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetHome_home_articles {
  __typename: "Article";
  title: string;
  slug: string | null;
  description: string | null;
  content: string | null;
  writer: GetHome_home_articles_writer | null;
  category: GetHome_home_articles_category | null;
  image: GetHome_home_articles_image | null;
}

export interface GetHome_home_Sectionabout_image {
  __typename: "UploadFile";
  url: string;
}

export interface GetHome_home_Sectionabout {
  __typename: "ComponentPageImageText";
  title: string | null;
  subTitle: string | null;
  description: string | null;
  image: GetHome_home_Sectionabout_image | null;
}

export interface GetHome_home_sectionCTA_links {
  __typename: "ComponentPageMenu";
  label: string;
  url: string;
}

export interface GetHome_home_sectionCTA {
  __typename: "ComponentPageCta";
  title: string | null;
  text: string | null;
  links: (GetHome_home_sectionCTA_links | null)[] | null;
}

export interface GetHome_home {
  __typename: "Home";
  banners: GetHome_home_banners[];
  reviews: GetHome_home_reviews[];
  services: GetHome_home_services[];
  articles: GetHome_home_articles[];
  Sectionabout: GetHome_home_Sectionabout | null;
  sectionCTA: GetHome_home_sectionCTA | null;
}

export interface GetHome {
  home: GetHome_home | null;
}
