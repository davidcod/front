/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ServiceFragment
// ====================================================

export interface ServiceFragment_image {
  __typename: "UploadFile";
  url: string;
}

export interface ServiceFragment {
  __typename: "Service";
  name: string | null;
  image: ServiceFragment_image | null;
  description: string | null;
}
