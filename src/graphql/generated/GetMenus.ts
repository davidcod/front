/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetMenus
// ====================================================

export interface GetMenus_menus {
  __typename: "Menu";
  name: string;
  url: string | null;
}

export interface GetMenus {
  menus: GetMenus_menus[];
}
