/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CTAFragment
// ====================================================

export interface CTAFragment_links {
  __typename: "ComponentPageMenu";
  label: string;
  url: string;
}

export interface CTAFragment {
  __typename: "ComponentPageCta";
  title: string | null;
  text: string | null;
  links: (CTAFragment_links | null)[] | null;
}
