import styled, {css} from 'styled-components'

export const SectionHero = styled.section`
    ${({theme}) =>css`
        background: ${theme.colors.mainBg};
        padding: ${theme.spacings.xxlarge} 0;
        color: ${theme.colors.white};
        text-align: center;
    
    `}
`
export const Image = styled.img`
    ${({theme}) =>css`
        max-width: 30rem;
        width: 100%;
    `}
`


export const Title = styled.h1`
    ${({theme}) =>css`
        margin: ${theme.spacings.small} 0;
        font-size: ${theme.font.sizes.huge};
    `}
`
export const Content = styled.p`
    ${({theme}) =>css`
        line-height: ${theme.font.sizes.medium};
        font-size: ${theme.font.sizes.medium};
    `}
`
