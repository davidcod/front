import Base from "templates/Base";
import { Container } from "components/Container";

const imageWhale = "/img/whale.png";

import * as S from "./styles";

export default function NotFoundTemplate() {
  return (
    <Base>
      <S.SectionHero>
        <Container>
          <S.Image src={imageWhale} />
          <S.Title>Ops</S.Title>
          <S.Content>
            Parece que a página que você está procurando não existe :/
          </S.Content>
        </Container>
      </S.SectionHero>
    </Base>
  );
}
