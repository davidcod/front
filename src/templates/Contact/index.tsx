import Base from "templates/Base";
import { Container } from "components/Container";
import Button from "components/Button";
import FormContact from "components/FormContact";

import * as S from "./styles";
import Heading from "components/Heading";

const ContactTemplate = () => (
  <Base>
    <S.Wrapper>
      <S.SectionHero>
        <Container>
          <S.Title color="white">Contact</S.Title>
        </Container>
      </S.SectionHero>
      <Container>
        <S.SectionContent>
          <S.SectionImage>
            <S.Image></S.Image>
          </S.SectionImage>
          <S.SectionForm>
            <Heading lineLeft color="black" lineColor="secondary">
              Contact Us
            </Heading>
            <FormContact />
          </S.SectionForm>
        </S.SectionContent>
      </Container>
    </S.Wrapper>
  </Base>
);

export default ContactTemplate;
