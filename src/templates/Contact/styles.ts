import styled, {css, DefaultTheme} from 'styled-components'

export const Wrapper = styled.main``

const wrapperModifiers = {
    white: (theme: DefaultTheme) => css`
        color: ${theme.colors.white};
    `,
    black: (theme: DefaultTheme) => css`
        color: ${theme.colors.black};
    `
}

export const SectionHero = styled.section`
    ${({theme}) =>css`
        background: ${theme.colors.mainBg};
        height: 38rem;
        display: flex;
        align-items: center;
        justify-content: center;
    `}
`
type TitleProps = {
    color?: 'white' | 'black'
}

export const Title = styled.h1<TitleProps>`
    ${({theme, color})=>css`        
        font-size: ${theme.font.sizes.huge};
        text-align: center;        

        ${!!color && wrapperModifiers.white(theme)};
        ${!!color && wrapperModifiers.black(theme)};
    `}
`

export const SectionContent = styled.section`
    ${({theme}) =>css`       
       margin-top: ${theme.spacings.large};
       display: grid;
       grid-template-columns: repeat(auto-fit, minmax(38rem, 1fr));
       gap: ${theme.spacings.xsmall};
       height: 100vh;
    `}
`
export const SectionImage = styled.div`   
`

export const Image = styled.div`
    ${({theme}) =>css`       
      background: ${theme.colors.mainBg};
      height: 47rem; 
      width: 100%;
    `}
`
export const SectionForm = styled.div`    
`

