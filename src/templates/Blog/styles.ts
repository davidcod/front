import styled, {css} from 'styled-components'
import media from 'styled-media-query'

export const SectionHero = styled.section`
    ${({theme}) =>css`
        background: ${theme.colors.mainBg};
        height: 38rem;
        display: flex;
        justify-content: center;
        align-items: center;
    `}
`

export const Title = styled.h1`
    ${({theme}) =>css`
        color: ${theme.colors.white};
        text-align: center;
        font-size: ${theme.font.sizes.huge};
    `}
`

export const SectionPosts = styled.main`
    ${({theme}) =>css`
        display: block;        

        ${media.greaterThan("small")`
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(33rem, 1fr));
        grid-gap: ${theme.spacings.medium};
        `}

        img{
            width: 100%;
        }
    `}
   
`

export const ShowMore = styled.button`
    ${({theme}) =>css`        
        border: 1px solid ${theme.colors.white};
        padding: ${theme.spacings.xsmall} ${theme.spacings.small};
        background: ${theme.colors.primary};
        color: ${theme.colors.white};
        transition: ${theme.transition.default};
        cursor: pointer;
        border-radius: ${theme.border.radius};
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 0 auto;

        &:hover{
            background: ${theme.colors.secondary};
        }
    `}
`

