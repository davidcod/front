import Link from "next/link";

import { ArrowLeft } from "@styled-icons/bootstrap/ArrowLeft";

import Base from "templates/Base";
import { Container } from "components/Container";

import * as S from "./styles";

export type ArticleSingleProps = {
  img: string;
  title: string;
  content: string;
  nameAuthor: string;
  photoAuthor: string;
  category: string;
};

const ArticleTemplate = ({
  img,
  title,
  content,
  nameAuthor,
  photoAuthor,
  category,
}: ArticleSingleProps) => (
  <Base>
    <S.Wrapper>
      <Container>
        <S.Image src={img} alt={title} />

        <S.CardTitle>{title}</S.CardTitle>
        <S.InfosBlock>
          <S.AuthorBlock>
            <S.AuthorImage src={photoAuthor} alt={nameAuthor} />
            <S.AuthorName>{nameAuthor}</S.AuthorName>
          </S.AuthorBlock>
          <S.Category>{category}</S.Category>
          <Link href={"/blog"}>
            <S.Back>
              <ArrowLeft size={24} />
              <span>Go Back</span>
            </S.Back>
          </Link>
        </S.InfosBlock>

        <S.Content dangerouslySetInnerHTML={{ __html: content }} />
      </Container>
    </S.Wrapper>
  </Base>
);

export default ArticleTemplate;
