import styled, {css} from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
    ${({theme}) => css`
        color: ${theme.colors.primary};
        padding: ${theme.spacings.xsmall}; 
        margin-right: ${theme.spacings.xsmall};
    `}
`

export const CardLink = styled.a`
    ${({theme}) => css`
        color: ${theme.colors.primary};
        text-decoration: none; 
    `}
`

export const Image = styled.img`
    height: 58rem;   
    object-fit: cover;   
    width: 100%;

    ${media.lessThan("small")`
        height: 100%;
    `}


`
export const InfosBlock = styled.div`
    ${({theme}) =>css`
        display: flex;
        align-items: center;
        margin: ${theme.spacings.xxsmall} 0;
    `}    
`

export const AuthorBlock = styled.div`
    ${({theme}) =>css`
        display: flex;
        align-items: center;
        margin-right: ${theme.spacings.xxsmall};
    `}
`

export const AuthorImage = styled.img`
    height: 2rem;   
    border-radius: 50%;   
    width: 2rem !important;
    margin-right: 1rem;
`


export const AuthorName = styled.div`
    ${({theme}) =>css`
        color: ${theme.colors.gray};
        font-size: ${theme.font.sizes.xsmall};
    `}
`

export const Category = styled.div`
    ${({theme}) =>css`
        background: ${theme.colors.secondary};
        padding: ${theme.spacings.xxsmall};
        border-radius: ${theme.border.radius};
        color: ${theme.colors.white};
        font-size: ${theme.font.sizes.xsmall};
    `}
`

export const Back = styled.a`
${({theme}) =>css`
    color: ${theme.colors.darkGray};
    display: flex;
    align-items: center;
    cursor: pointer;
    justify-content: flex-end;
    margin-left: ${theme.spacings.xsmall};

    >span{
        text-transform: uppercase;
        font-size: ${theme.font.sizes.xsmall};
        margin-left: ${theme.spacings.xxsmall};
    }
`}
`

export const CardTitle = styled.h1`
    ${({theme}) => css`
        font-size: ${theme.font.sizes.xxlarge};
        margin-top: ${theme.spacings.small};
        margin-bottom: ${theme.spacings.xxsmall};
    `}
`

export const Content = styled.p`
    ${({theme}) => css`        
        font-size: ${theme.font.sizes.medium};
        color: ${theme.colors.darkGray};
        line-height: ${theme.font.sizes.xxlarge};
    `}
`
