import { useQuery } from "@apollo/client";
import { KeyboardArrowDown as ArrowDown } from "@styled-icons/material-outlined/KeyboardArrowDown";

import { Container } from "components/Container";
import PostList from "components/PostList";
import Card, { CardProps } from "components/Card";
import Base from "templates/Base";
import Pagination, { PaginationProps } from "components/Pagination";
import {
  GetArticles,
  GetArticlesVariables,
} from "graphql/generated/GetArticles";
import { GET_ARTICLES } from "graphql/queries/articles";

import * as S from "./styles";

export type BlogProps = {
  posts: CardProps[];
};

const BlogTemplate = ({ posts }: BlogProps) => {
  const { data, fetchMore } = useQuery<GetArticles, GetArticlesVariables>(
    GET_ARTICLES,
    { variables: { limit: 4 } }
  );

  const handleShowMore = () => {
    fetchMore({ variables: { limit: 6, start: data?.articles.length } });
  };
  return (
    <Base>
      <S.SectionHero>
        <S.Title>Blog</S.Title>
      </S.SectionHero>
      <Container>
        <S.SectionPosts>
          {data?.articles.map((post) => (
            <Card
              key={post.title}
              title={post.title}
              category={post.category?.name}
              img={`http://localhost:1337${post.image?.url}`}
              nameAuthor={post.writer?.name}
              photoAuthor={`http://localhost:1337${post.writer?.photo?.url}`}
            />
          ))}
        </S.SectionPosts>
        <S.ShowMore role="button" onClick={handleShowMore}>
          <p>Show More</p>
          <ArrowDown size={35} />
        </S.ShowMore>
      </Container>
    </Base>
  );
};

export default BlogTemplate;
