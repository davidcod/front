import { initializeApollo } from "utils/apollo";
import { GET_MENUS } from "graphql/queries/menus";
import { GetMenus } from "graphql/generated/GetMenus";

import Menu from "components/Menu";
import { Container } from "components/Container";
import Footer from "components/Footer";

import * as S from "./styles";

export type BaseTemplateProps = {
  children: React.ReactNode;
};

const Base = ({ children }: BaseTemplateProps) => (
  <section>
    <Container>
      <Menu />
    </Container>
    {children}
    <Container>
      <Footer />
    </Container>
  </section>
);

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query<GetMenus>({
    query: GET_MENUS,
  });

  return {
    props: {
      revalidate: 1,
      links: data.menus.map((link) => ({
        name: link.name,
        url: link.url,
      })),
    },
  };
}

export default Base;
