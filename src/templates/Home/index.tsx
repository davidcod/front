import Base from "templates/Base";
import { Container } from "components/Container";
import { BannerProps } from "components/Banner";
import BannerSlider from "components/BannerSlider";
import SectionAboutProject, {
  SectionAboutProps,
} from "components/SectionAboutProject";
import ServiceList from "components/ServiceList";
import { ServiceCardProps } from "components/ServiceCard";
import SectionReviews, { ReviewSliderProps } from "components/SectionReviews";
import { ReviewCardProps } from "components/ReviewCard";
import { CardProps } from "components/Card";
import PostList from "components/PostList";
import SectionCTA, { SectionCTAProps } from "components/SectionCta";

import * as S from "./styles";

export type HomeTemplateProps = {
  banners: BannerProps[];
  sectionAbout: SectionAboutProps;
  services: ServiceCardProps[];
  reviews: ReviewCardProps[];
  posts: CardProps[];
  homecta: SectionCTAProps;
};

const Home = ({
  banners,
  sectionAbout,
  services,
  reviews,
  posts,
  homecta,
}: HomeTemplateProps) => (
  <Base>
    <S.SectionBanner>
      <Container>
        <BannerSlider items={banners} />
      </Container>
    </S.SectionBanner>
    <S.SectionAbout>
      <Container>
        <SectionAboutProject {...sectionAbout} />
      </Container>
    </S.SectionAbout>

    <S.SectionServices>
      <Container>
        <ServiceList items={services} />
      </Container>
    </S.SectionServices>

    <S.SectionCTA>
      <Container>
        <SectionCTA {...homecta} />
      </Container>
    </S.SectionCTA>

    <S.SectionReviews>
      <Container>
        <SectionReviews items={reviews} />
      </Container>
    </S.SectionReviews>

    <S.SectionBlog>
      <Container>
        <PostList items={posts} />
      </Container>
    </S.SectionBlog>
  </Base>
);

export default Home;
