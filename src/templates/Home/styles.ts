import styled, { css } from 'styled-components'
import media from 'styled-media-query'

import * as HeadingStyles from 'components/Heading/styles'

export const SectionBanner = styled.section`
  ${({ theme }) => css`
    margin: 0 calc(-${theme.grid.gutter} / 2) ${theme.spacings.large};
    clip-path: polygon(0 0, 100% 15%, 100% 100%, 0 85%);

    ${media.greaterThan('medium')`
      margin: ${theme.spacings.large} 0;
      position: relative;
      z-index: ${theme.layers.base};
    `}
    background: ${theme.colors.primary};    
    padding: calc(${theme.spacings.large} * 4) 0;
  `}
`

export const SectionAbout = styled.section`
  ${({ theme }) => css`
    margin-bottom: ${theme.spacings.large};   

    ${media.greaterThan('large')`
      margin-top: -13rem;
    `}

    ${media.greaterThan('medium')`
      margin-bottom: 0;
      padding-top: 14rem;
      padding-bottom: 10rem;
      background-color: ${theme.colors.lightBg};

      ${HeadingStyles.Wrapper} {
        color: ${theme.colors.black};
      }
    `}
  `}
`

export const SectionReviews = styled.section`
  ${({ theme }) => css`
    padding: ${theme.spacings.xxlarge} 0; 
  `}
`

export const SectionServices = styled.section`
  ${({ theme }) => css`
    padding: calc(${theme.spacings.large} * 4) 0;
    background: ${theme.colors.mainBg};
    //clip-path: polygon(0 15%, 100% 15%, 100% 100%, 0 85%);
  `}
`

export const SectionCTA = styled.section`
  ${({ theme }) => css`
    margin-bottom: ${theme.spacings.large};   
    background: ${theme.colors.secondary};  
    padding: ${theme.spacings.large} 0;    
  `}
`

export const SectionBlog = styled.section`
  ${({ theme }) => css`
    padding: ${theme.spacings.xxlarge} 0;     
    background: ${theme.colors.white};

    ${media.lessThan('small')`
      padding: ${theme.spacings.large} 0;
    `}
  `}
`
