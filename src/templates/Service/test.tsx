import { render, screen } from '@testing-library/react'

import Service from '.'

describe('<Service />', () => {
  it('should render the heading', () => {
    const { container } = render(<Service />)

    expect(screen.getByRole('heading', { name: /Service/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
