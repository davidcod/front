import styled, {css} from 'styled-components'

export const SectionHero = styled.section`
    ${({theme}) =>css`
        background: ${theme.colors.mainBg};
        padding: ${theme.spacings.large} 0;
    `}
`

export const Title = styled.h1`
    ${({theme}) =>css`
        color: ${theme.colors.white};
        text-align: center;
        font-size: ${theme.font.sizes.huge};
    `}
`
