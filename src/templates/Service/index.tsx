import Base from "templates/Base";
import { Container } from "components/Container";

import * as S from "./styles";

const ServiceTemplate = () => (
  <Base>
    <S.SectionHero>
      <S.Title>Our Services</S.Title>
    </S.SectionHero>
    <Container>Content...</Container>
  </Base>
);

export default ServiceTemplate;
