import { Story, Meta } from '@storybook/react'
import Service from '.'

export default {
  title: 'Service',
  component: Service
} as Meta

export const Default: Story = () => <Service />
