import styled, {css} from 'styled-components'
import media from 'styled-media-query'

import * as HeadingStyles from 'components/Heading/styles'


export const SectionAbout = styled.section`
  ${({ theme }) => css`
    margin-bottom: ${theme.spacings.large};   

    ${media.greaterThan('large')`
      margin-top: -13rem;
    `}

    ${media.greaterThan('medium')`
      margin-bottom: 0;
      padding-top: 14rem;
      padding-bottom: 10rem;
      background-color: ${theme.colors.lightBg};

      ${HeadingStyles.Wrapper} {
        color: ${theme.colors.black};
      }
    `}
  `}
`

export const SectionService = styled.section`
  ${({ theme }) => css`
    margin-bottom: ${theme.spacings.large}; 
    background: ${theme.colors.mainBg};
    padding: ${theme.spacings.large} 0;  
   
  `}
`

export const SectionReviews = styled.section`
  ${({ theme }) => css`
    padding: calc(${theme.spacings.large} * 4) 0;
    background: ${theme.colors.mainBg};
    //clip-path: polygon(0 15%, 100% 15%, 100% 100%, 0 85%);
  `}
`
