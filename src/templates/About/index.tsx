import Base from "templates/Base";
import { Container } from "components/Container";
import BlockText from "components/BlockText";
import SectionAboutProject, {
  SectionAboutProps,
} from "components/SectionAboutProject";

import * as S from "./styles";
import Banner, { BannerProps } from "components/Banner";
import { ServiceCardProps } from "components/ServiceCard";
import ServiceList, { ServicesProps } from "components/ServiceList";
import { ReviewCardProps } from "components/ReviewCard";
import SectionReviews from "components/SectionReviews";

export type AboutTemplateProps = {
  banner: BannerProps;
  aboutUs: SectionAboutProps;
  serviceList: ServiceCardProps[];
};

export default function About({
  banner,
  aboutUs,
  serviceList,
}: AboutTemplateProps) {
  return (
    <Base>
      <Banner {...banner} />
      <S.SectionAbout>
        <Container>
          <SectionAboutProject {...aboutUs} />
        </Container>
      </S.SectionAbout>
      <S.SectionService>
        <Container>
          <ServiceList items={serviceList} />
        </Container>
      </S.SectionService>
    </Base>
  );
}
