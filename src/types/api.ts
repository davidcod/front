//Homepage
export type Banner = {
    title?: string
    subtitle?: string
    image: {
        url: string
    }       
}

export type SectionBannerProps = {
    banners: Banner[]
}

export type HomeProps = {
    sectionBanners: SectionBannerProps
}


//About page

export type SectionHeroProps = {
    hero: Banner
}

export type SectionAboutUsProps = {
    title: string
    subTitle: string
    description: string
    image: {
        url: string
    }
}
export type AboutProps = {
    sectionHero: SectionHeroProps
    sectionAbout: SectionAboutUsProps
}