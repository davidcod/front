import { GetHome } from "graphql/generated/GetHome";
import { GET_HOME } from "graphql/queries/home";
import { initializeApollo } from "utils/apollo";
import Home, { HomeTemplateProps } from "templates/Home";

export default function Index(props: HomeTemplateProps) {
  return <Home {...props} />;
}

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query<GetHome>({
    query: GET_HOME,
  });

  return {
    props: {
      revalidate: 60,
      banners: data.home?.banners.map((banner) => ({
        img: banner.image?.url,
        title: banner.title,
        subtitle: banner.subtitle,
        buttonLabel: banner.button?.label,
        buttonLink: banner.button?.url,
      })),

      sectionAbout: {
        image: `http://localhost:1337${data.home?.Sectionabout?.image?.url}`,
        title: data.home?.Sectionabout?.title,
        subtitle: data.home?.Sectionabout?.subTitle,
        description: data.home?.Sectionabout?.description,
      },

      services: data.home?.services.map((service) => ({
        name: service.name,
        image: service.image?.url,
        description: service.description,
      })),

      homecta: {
        title: data.home?.sectionCTA?.title,
        text: data.home?.sectionCTA?.text,
        links: data.home?.sectionCTA?.links?.map((link) => ({
          label: link?.label,
          url: link?.url,
        })),
      },

      reviews: data.home?.reviews.map((review) => ({
        name: review.name,
        image: review.image?.url,
        description: review.description,
      })),

      posts: data.home?.articles.map((article) => ({
        title: article.title,
        slug: article.slug,
        img: `http://localhost:1337${article.image?.url}`,
        nameAuthor: article.writer?.name,
        photoAuthor: `http://localhost:1337${article.writer?.photo?.url}`,
        description: article.description,
        category: article.category?.name,
      })),
    },
  };
}
