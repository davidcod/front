import { useRouter } from "next/router";
import { GetStaticProps } from "next";

import { initializeApollo } from "utils/apollo";
import { GetArticles } from "graphql/generated/GetArticles";
import { GET_ARTICLES } from "graphql/queries/articles";
import { ARTICLE_BY_SLUG } from "graphql/queries/article_by_slug";
import { ArticleBySlug } from "graphql/generated/ArticleBySlug";

import ArticleTemplate, { ArticleSingleProps } from "templates/Blog/Article";

const apolloClient = initializeApollo();

export default function Article(props: ArticleSingleProps) {
  const router = useRouter();

  if (router.isFallback) return null;

  return <ArticleTemplate {...props} />;
}

export async function getStaticPaths() {
  const { data } = await apolloClient.query<GetArticles>({
    query: GET_ARTICLES,
    variables: { limit: 9, start: 9 },
  });

  const paths = data.articles.map(({ slug }) => ({
    params: { slug },
  }));

  return {
    paths,
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { data } = await apolloClient.query<ArticleBySlug>({
    query: ARTICLE_BY_SLUG,
    variables: { slug: `${params?.slug}` },
  });

  if (!data.articles.length) {
    return { notFound: true };
  }

  const article = data.articles[0];

  return {
    props: {
      revalidade: 60,
      img: `http://localhost:1337${article.image?.url}`,
      title: article.title,
      slug: article.slug,
      nameAuthor: article.writer?.name,
      photoAuthor: `http://localhost:1337${article.writer?.photo?.url}`,
      category: article.category?.name,
      content: article.content,
    },
  };
};
