import { initializeApollo } from "utils/apollo";
import { GetAbout } from "graphql/generated/GetAbout";
import { GET_ABOUT } from "graphql/queries/about";

import About, { AboutTemplateProps } from "templates/About";

export default function AboutPage(props: AboutTemplateProps) {
  return <About {...props} />;
}

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query<GetAbout>({
    query: GET_ABOUT,
  });

  return {
    props: {
      revalidate: 60,
      banner: {
        title: data.about?.banners?.title,
        subtitle: data.about?.banners?.subtitle,
        img: data.about?.banners?.image?.url,
        buttonLabel: data.about?.banners?.button?.label,
        buttonUrl: data.about?.banners?.button?.url,
      },

      aboutUs: {
        image: `http://localhost:1337${data.about?.aboutUs?.image?.url}`,
        title: data.about?.aboutUs?.title,
        subtitle: data.about?.aboutUs?.subTitle,
        description: data.about?.aboutUs?.description,
      },

      serviceList: data.about?.serviceList?.service?.map((service) => ({
        name: service?.name,
        image: service?.icon?.url,
      })),
    },
  };
}
