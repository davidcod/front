import {
  GetArticles,
  GetArticlesVariables,
} from "graphql/generated/GetArticles";
import { GET_ARTICLES } from "graphql/queries/articles";
import { initializeApollo } from "utils/apollo";

import BlogTemplate, { BlogProps } from "templates/Blog";

export default function BlogPage(props: BlogProps) {
  return <BlogTemplate {...props} />;
}

export async function getStaticProps() {
  const apolloClient = initializeApollo();
  await apolloClient.query<GetArticles, GetArticlesVariables>({
    query: GET_ARTICLES,
    variables: { limit: 6 },
  });

  return {
    props: {
      revalidate: 60,
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}
